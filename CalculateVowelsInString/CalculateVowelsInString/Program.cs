﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CalculateVowelsInString
{
    /// <summary>
    /// This class is to find the no of vowels
    /// Given by the string from user
    /// </summary>
    class Program
    {
        /// <summary>
        /// Contains all the Vowel Values
        /// </summary>
        public enum Vowels
        {
            a, e, i, o, u, A, E, I, O, U
        };
       
        /// <summary>
        /// Main methods gets the string from user
        /// Calles the CalculateVowelCount() function
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
   
            Console.WriteLine("Enter a string");  
            string inputStr = Console.ReadLine();  //Getting string from the user
            CalculateVowelCount(inputStr);         //Calling this function to calculate the no of vowels
            Console.ReadKey();


        }
       /// <summary>
       /// this method calculates the no of vowels present in the given string
       /// </summary>
       /// <param name="inputStr"></param>
       static void CalculateVowelCount(String inputStr)
        {
            int vowelCount=0;
            for(int i=0;i<inputStr.Length;i++)
            {
                int choice=0;
                if (Enum.IsDefined(typeof(Vowels), Char.ToString(inputStr[i])))  //Checking whether the character present in the Vowel Enum
                    choice = 1;  // If present Case 1 is called.

                switch(choice)
                {
                    case  1:
                    {
                        vowelCount++; break;  // Incrementing the vowelCount is the character is a vowel.
                    }
                    default: break;
                }

            }
            Console.WriteLine("\n No of vowels in the input String : {0} ",vowelCount);  // Displaying the result

        }

    }
}
