﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwapTwoNumbers
{
    /// <summary>
    /// This class gets two inputs from the user
    /// swaps the two inputs
    /// Displays it to the user
    /// </summary>
    class Program
    {
        /// <summary>
        /// Gets two inputs from the user
        /// Calls the swap function
        /// Displays the result
        /// </summary>
        static void Main()
        {
            int num1, num2;
            Console.WriteLine("Enter 2 values : ");
            num1 = Convert.ToInt32(Console.ReadLine());
            num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Before swap : Num1 = {0} Num2 = {1} ", num1, num2);
            var numTuple = Swap(num1, num2);
            Console.WriteLine("After  swap : Num1 = {0} Num2 = {1} ", numTuple.Item1, numTuple.Item2);


        }
        /// <summary>
        /// This method performs swap operation
        /// </summary>
        /// <param name="num1">
        /// 1st number
        /// </param>
        /// <param name="num2">
        /// 2nd number
        /// </param>
        /// <returns>
        /// Returns a tuple consists of 2 swapped values
        /// </returns>
        static Tuple<int,int> Swap(int num1, int num2)
        {
            int temp;
            temp = num1;
            num1 = num2;
            num2 = temp;
            return new Tuple<int,int>(num1, num2);

        }
    }
}
