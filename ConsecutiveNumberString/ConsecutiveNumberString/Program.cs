﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ConsecutiveNumberString
{

    /// <summary>
    /// This class gets a numeric string of 5 values seperated by '-'
    /// Checks the values entered are consecutive or not
    /// </summary>
    class Program
    {

        /// <summary>
        /// Main methods gets the input from user
        /// calls the GetValues() method and CheckConsecutive() method
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            string mainStr;
            Console.WriteLine("Enter a string of 5 numbers seperated by '-' (Eg: 1-2-3-4-5) "); //Getting input from the user
            mainStr = Console.ReadLine(); //Setting the value to the string
            CheckConsecutive(GetValues(mainStr)); //Calling the functions
        }


        /// <summary>
        /// This method seperates the numbers from the string
        /// </summary>
        /// <param name="mainStr"></param>
        /// <returns>
        /// Returns a list of integer values
        /// </returns>
        public static List<int> GetValues(string mainStr)
        {
            List<int> numList = new List<int>();
            int currNum = 0;
            for (int i = 0; i < mainStr.Length; i++)
            {

                if (mainStr[i] != '-') //Checking the character is not  equal to "-"
                {
                    currNum = (currNum * 10) + (mainStr[i] - 48); //Updating the current value
                }

                else // If character is "-" , currValue added to the list
                {
                    numList.Add(currNum);
                    currNum = 0; //Resetting currNum
                }

                if (i == mainStr.Length - 1) numList.Add(currNum); //Checking the string reaches the end

            }

            return numList; //Returning the list
        }

        /// <summary>
        /// This function checks whether the list of integers are consecutive or not
        /// </summary>
        /// <param name="numList"></param>
        public static void CheckConsecutive(List<int> numList)
        {
            int[] numArray = numList.ToArray(); //Converting the intlist to array of int 
            int[] checkArray = new int[numArray.Length]; //Initializing other array for check
            Boolean checkFlag = true;

            for (int i = 1; i < numArray.Length; i++)
            {
                checkArray[i] = numArray[i] - numArray[i - 1]; //Storing the differnce between adjacent values of the array

            }
            for (int i = 2; i < checkArray.Length; i++)
            {
                if (checkArray[i] != checkArray[1]) //Checking the difference array elements are equal
                {
                    checkFlag = false; break; //Updating the flag
                }
            }
            if (checkFlag)  //Displaying the result
                Console.WriteLine("Consecutive");
            else
                Console.WriteLine("Not consecutive");

        }
    }
}
