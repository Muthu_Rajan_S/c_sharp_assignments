﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveVowelsFromStringArray
{
    /// <summary>
    /// This class gets n no of strings from user
    /// Removes all the Vowels from each string
    /// Returns the each strings without vowels 
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main methods gets the words from user
        /// stores it into an array of strings
        /// calls the RemoveVowels fuction
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            Console.WriteLine("Enter number of elements to enter :");
            int numOfElements = int.Parse(Console.ReadLine());            //Getting no of elements from user

            string[] mainStr = new string[numOfElements];

            Console.WriteLine("Enter " + numOfElements + " elements one by one :");
            for (int i = 0; i < numOfElements; i++)
            {
                mainStr[i] = Console.ReadLine();                     // Getting individual words from user

            }
            RemoveVowels(mainStr);                                  // Calling the RemoveVowels() function   


        }

        /// <summary>
        /// This function takes array of strings as input
        /// Removes all vowels from each string
        /// Prints each string without vowels
        /// </summary>
        /// <param name="mainStr"></param>
        static void RemoveVowels(string[] mainStr)   // Takes string array as parameter
        {
            string currStr, vowelStr = "aeiouAEIOU";   //vowelStr consists of all vowel Characters

            Console.WriteLine("\nElements without Vowels : \n");
            for (int i = 0; i < mainStr.Length; i++)
            {
                String modifiedStr = "";
                currStr = mainStr[i];              //Assigning curr word to currStr
                for (int j = 0; j < currStr.Length; j++)
                {
                    if (!vowelStr.Contains(currStr[j]))       // Checking whether the character of each word is present in the vowel string
                        modifiedStr = String.Concat(modifiedStr, currStr[j]);  //If not present, the modifiedStr concatenates with current Character
                }
                Console.WriteLine("Element " + i + "-> " + modifiedStr);     //Displaying every element without vowels
            }
        }
    }
}
 